# Install

First, install `ingress-nginx`:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/cloud/deploy.yaml
```

Then, configure ingress:

```bash
kubectl apply -f ingress/ingress.yaml
kubectl apply -f ingress/ingress-auth.yaml
```